package br.com.acessocontroledeacessos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerControleDeAcessosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumerControleDeAcessosApplication.class, args);
	}

}
