package br.com.acessocontroledeacessos.consumers;

import br.com.acessocontroledeacessos.producers.LogAcesso;
import br.com.acessocontroledeacessos.services.LogAcessoService;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class LogAcessoConsumer {

    @Autowired
    private LogAcessoService logAcessoService;

    @KafkaListener(topics = "spec3-marco-aurelio-1", groupId = "marcosimoes-1")
    public void receber(@Payload LogAcesso logAcesso) throws CsvRequiredFieldEmptyException, IOException, CsvDataTypeMismatchException {
        System.out.println("marcosimoes: Recebi a requisição de acesso do cliente " +
                logAcesso.getClienteId() + " para a porta " + logAcesso.getPortaId() +
                ". Permissão de acesso = " + logAcesso.isAcessoPermitido());

        logAcessoService.montaArquivoCSV(logAcesso);
    }
}
