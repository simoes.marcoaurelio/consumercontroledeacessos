package br.com.acessocontroledeacessos.producers;

import java.time.LocalDate;
import java.time.LocalTime;

public class LogAcesso {

    private int clienteId;
    private int portaId;
    private boolean acessoPermitido;
    //    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDate dataDoAcesso;
    //    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalTime horaDoAceeso;

    public LogAcesso() {
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public int getPortaId() {
        return portaId;
    }

    public void setPortaId(int portaId) {
        this.portaId = portaId;
    }

    public boolean isAcessoPermitido() {
        return acessoPermitido;
    }

    public void setAcessoPermitido(boolean acessoPermitido) {
        this.acessoPermitido = acessoPermitido;
    }

    public LocalDate getDataDoAcesso() {
        return dataDoAcesso;
    }

    public void setDataDoAcesso(LocalDate dataDoAcesso) {
        this.dataDoAcesso = dataDoAcesso;
    }

    public LocalTime getHoraDoAceeso() {
        return horaDoAceeso;
    }

    public void setHoraDoAceeso(LocalTime horaDoAceeso) {
        this.horaDoAceeso = horaDoAceeso;
    }
}