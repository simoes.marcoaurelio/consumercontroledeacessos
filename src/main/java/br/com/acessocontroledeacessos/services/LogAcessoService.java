package br.com.acessocontroledeacessos.services;

import br.com.acessocontroledeacessos.producers.LogAcesso;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

@Service
public class LogAcessoService {

    public void montaArquivoCSV(LogAcesso logAcesso)
            throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        Writer writer = new FileWriter(
                "/home/a2/Documentos/workspace/sistemaControleDeAcessos/arquivosDeLog/LogAcesso.csv",
                true );
        StatefulBeanToCsv<LogAcesso> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(logAcesso);

        writer.flush();
        writer.close();
    }
}
